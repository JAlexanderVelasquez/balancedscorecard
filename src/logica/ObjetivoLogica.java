/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Indicador;
import modelo.Objetivo;
import modelo.Usuario;
import persistencia.ObjetivoJpaController;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class ObjetivoLogica {
    
    private ObjetivoJpaController objCon=new ObjetivoJpaController();
    private IndicadorLogica indLog=new IndicadorLogica();
    
    public ObjetivoLogica(){
        
    }
    
    public Objetivo getObjetivo(int id){
        Objetivo obj=objCon.findObjetivo(id);
        if(obj==null)
            JOptionPane.showMessageDialog(null, "Error, Objetivo no encontrado");
        return obj;
    }
    
     public Objetivo registrarObjetivo(Usuario user, String area, String des){
        Objetivo obj=new Objetivo();
        obj.setArea(area);
        obj.setCreadorObjetivo(user);
        obj.setDescripcion(des);
        obj.setFecha(new Date());
        
        try {
            objCon.create(obj);
            System.out.println(obj.getArea());
            System.out.println(obj.getCreadorObjetivo());
            System.out.println(obj.getFecha());
            System.out.println(obj.getId());
            System.out.println(obj.getDescripcion());
            JOptionPane.showMessageDialog(null, "Objetivo registrado con exito");
            return obj;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al registrar objetivo");
        }
        
        return null;
        
    }
    public List<String> obtenerObjetivos(String Area){
        List<Objetivo> objss = objCon.findObjetivoEntities();
        List<String> Lobjs = new ArrayList<>();
        for(Objetivo o:objss){
            Lobjs.add(""+o.getDescripcion());
        }
        return Lobjs;
    }
     
    public List<Objetivo> obtenerObjetivoss(String Area){
        List<Objetivo> objss = objCon.findObjetivoEntities();
        List<Objetivo> Lobjs = new ArrayList<>();
        for(Objetivo o:objss){
            if(o.getArea().equals(Area)){
            Lobjs.add(o);
            }
        }
        return Lobjs;
    }
    public List<String> obtenerObjetivoss2(String Area){
        List<Objetivo> objss = objCon.findObjetivoEntities();
        List<String> Lobjs = new ArrayList<>();
        for(Objetivo o:objss){
            if(o.getArea().equals(Area)){
                Lobjs.add(o.getDescripcion());
            }
        }
        return Lobjs;
    }
    public List<Objetivo> obtenerObjetivossInd(String Area){
        List<Objetivo> objss = objCon.findObjetivoEntities();
        List<Objetivo> Lobjs = new ArrayList<>();
        for(Objetivo o:objss){
            if(o.getArea().equals(Area)){
                List<Indicador> inds= indLog.obtenerIndicadores2(o);
                for(Indicador i:inds){
                    Lobjs.add(o);
                }
            
            }
        }
        return Lobjs;
            
        
    }

    public boolean eliminarObjetivo(Objetivo objt) throws IllegalOrphanException {
        try {
            System.out.println("ELIMINAR OBJ");
            objCon.destroy(objt.getId());
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }

    public List<String> obtenerObjetivos2() {
        List<Objetivo> objss = objCon.findObjetivoEntities();
        List<String> Lobjss = new ArrayList<>();
        for(Objetivo u:objss){
            Lobjss.add(""+u.getDescripcion());
        }
        return Lobjss;
    }

    public Objetivo getObjetivo2(String descp) {
        List<Objetivo> objss = objCon.findObjetivoEntities();
        for(Objetivo u:objss){
            if(u.getDescripcion().equals(descp))
                return u;
        }
        return null;
    }

    public boolean editarObjetivo(Usuario userCreador, String area, String text, Objetivo obk) {
        obk.setDescripcion(text);
        try {
            objCon.edit(obk);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
