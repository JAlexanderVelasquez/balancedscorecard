/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Iniciativa;
import modelo.Meta;
import modelo.Objetivo;
import modelo.Usuario;
import persistencia.IniciativaJpaController;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class IniciativaLogica {
        private IniciativaJpaController iniCon=new IniciativaJpaController();
    
    public IniciativaLogica(){
        
    }
    
    public Iniciativa getIniciativa(int id){
        Iniciativa ini=iniCon.findIniciativa(id);
        if(ini==null)
            JOptionPane.showMessageDialog(null, "Error, Iniciativa no encontrado");
        return ini;
    }
    
     public Iniciativa registrarIniciativa(Usuario user,Objetivo obj, String area, String des){
        Iniciativa ini= new Iniciativa();
        ini.setArea(area);
        ini.setIniciativaObjetivo(obj);
        ini.setDescripcion(des);
        ini.setFecha(new Date());
        System.out.println(ini);
        try {
            iniCon.create(ini);
            JOptionPane.showMessageDialog(null, "Iniciativa registrada con exito");
            return ini;
        } catch (Exception ex) {
                        System.out.println(ex);

            JOptionPane.showMessageDialog(null, "Error al registrar iniciativa");
        }
        
        return null;
        
    }
    public List<String> obtenerIniciativas(String Area){
        List<Iniciativa> iniss = iniCon.findIniciativaEntities();
        List<String> Linis = new ArrayList<>();
        for(Iniciativa i:iniss){
            Linis.add(""+i.getDescripcion());
        }
        return Linis;
    }
    
    public List<Iniciativa> obtenerIniciativas2(Objetivo obj){
        List<Iniciativa> iniss = iniCon.findIniciativaEntities();
        List<Iniciativa> Linis = new ArrayList<>();
        for(Iniciativa i:iniss){
            if(obj.equals(i.getIniciativaObjetivo())){
                Linis.add(i);
            }
        }
        return Linis;
    }
    public List<String> obtenerIniciativas3(Objetivo obj){
        List<Iniciativa> iniss = iniCon.findIniciativaEntities();
        List<String> Linis = new ArrayList<>();
        for(Iniciativa i:iniss){
            if(i.getIniciativaObjetivo().equals(obj)){
                Linis.add(i.getDescripcion());
            }
        }
        return Linis;
    }
    public boolean eliminarIniciativa(Iniciativa ini) {
        try {
            iniCon.destroy(ini.getCodigo());
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    public Iniciativa getIniciativa2(String descp) {
        List<Iniciativa> metss = iniCon.findIniciativaEntities();
        for(Iniciativa u:metss){
            if(u.getDescripcion().equals(descp))
                return u;
        }
        return null;
    }

    public void editarIniciativa(Iniciativa ini, Usuario userCreador, Objetivo obj, String area, String text) {
        ini.setFecha(new Date());
        ini.setDescripcion(text);
        try {
            iniCon.edit(ini);
            JOptionPane.showMessageDialog(null, "Iniciativa editada con exito");
        } catch (Exception ex) {
            Logger.getLogger(MetaLogica.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error al editar Iniciativa");
        }
    }
}
