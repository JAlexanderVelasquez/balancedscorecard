/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Meta;
import modelo.Objetivo;
import modelo.Usuario;
import persistencia.MetaJpaController;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class MetaLogica {
    
    private MetaJpaController metCon=new MetaJpaController();
    
    public MetaLogica(){
        
    }
    
    public Meta getMeta(int id){
        Meta met=metCon.findMeta(id);
        if(met==null)
            JOptionPane.showMessageDialog(null, "Error, Meta no encontrado");
        return met;
    }
    
     public Meta registrarMeta(Usuario user,Objetivo obj, String area, String des){
         System.out.println(obj);
         System.out.println(obj.getArea());
         System.out.println(obj.getCreadorObjetivo());
         System.out.println(obj.getDescripcion());
         System.out.println(obj.getFecha());
         System.out.println(obj.getId());
        Meta met= new Meta();
        met.setArea(area);
        met.setMetaObjetivo(obj);
        met.setDescripcion(des);
        met.setFecha(new Date());
        met.setCodigo(0);
        System.out.println(met);
        try {
            metCon.create(met);
            JOptionPane.showMessageDialog(null, "Meta registrado con exito");
            return met;
        } catch (Exception ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Error al registrar meta");
        }
        
        return null;
        
    }
     public List<String> obtenerMetas(String Area){
        List<Meta> metss = metCon.findMetaEntities();
        List<String> Lmets = new ArrayList<>();
        for(Meta m:metss){
            Lmets.add(""+m.getDescripcion());
        }
        return Lmets;
    }
     public List<Meta> obtenerMetas2(Objetivo obj){
        List<Meta> metss = metCon.findMetaEntities();
        List<Meta> Lmets = new ArrayList<>();
        for(Meta m:metss){
            if(obj.equals(m.getMetaObjetivo())){
                Lmets.add(m);
            }
        }
        return Lmets;
    }
     public List<String> obtenerMetas3(Objetivo obj){
        List<Meta> metss = metCon.findMetaEntities();
        List<String> Lmets = new ArrayList<>();
        for(Meta m:metss){
            if(m.getMetaObjetivo().equals(obj)){
                Lmets.add(m.getDescripcion());
            }
        }
        return Lmets;
    }
    public boolean eliminarMeta(Meta met) {
        try {
            metCon.destroy(met.getCodigo());
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    public Meta getMeta2(String descp) {
        List<Meta> metss = metCon.findMetaEntities();
        for(Meta u:metss){
            if(u.getDescripcion().equals(descp))
                return u;
        }
        return null;
    }

    public void editarMeta(Meta met,Usuario userCreador, Objetivo obj, String area, String text) {
        met.setFecha(new Date());
        met.setDescripcion(text);
        try {
            metCon.edit(met);
            JOptionPane.showMessageDialog(null, "Meta editada con exito");
        } catch (Exception ex) {
            Logger.getLogger(MetaLogica.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error al editar meta");
        }
    }
}
