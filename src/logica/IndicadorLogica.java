/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Indicador;
import modelo.Iniciativa;
import modelo.Objetivo;
import modelo.Usuario;
import persistencia.IndicadorJpaController;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class IndicadorLogica {
        private IndicadorJpaController indiCon=new IndicadorJpaController();
    
    public IndicadorLogica(){
        
    }
    
    public Indicador getIndicador(int id){
        Indicador indi=indiCon.findIndicador(id);
        if(indi==null)
            JOptionPane.showMessageDialog(null, "Error, Indicador no encontrado");
        return indi;
    }
    
     public Indicador registrarIndicador(Usuario user,Objetivo obj, String area, String des){
        Indicador indi= new Indicador();
        indi.setArea(area);
        indi.setIndicadorObjetivo(obj);
        indi.setDescripcion(des);
        indi.setFecha(new Date());
        System.out.println(indi);
        try {
            indiCon.create(indi);
            JOptionPane.showMessageDialog(null, "Indicador registrada con exito");
            return indi;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al registrar indicador");
        }
        
        return null;
        
    }
     public List<String> obtenerIndicadores(String Area){
        List<Indicador> indiss = indiCon.findIndicadorEntities();
        List<String> Lindis = new ArrayList<>();
        for(Indicador i:indiss){
            Lindis.add(""+i.getDescripcion());
        }
        return Lindis;
    }
     public List<Indicador> obtenerIndicadores2(Objetivo obj){
        List<Indicador> indiss = indiCon.findIndicadorEntities();
        List<Indicador> Lindis = new ArrayList<>();
        for(Indicador i:indiss){
            if(obj.equals(i.getIndicadorObjetivo())){
                Lindis.add(i);
            }
        }
        return Lindis;
    }
      public List<String> obtenerIndicadores3(Objetivo obj){
        List<Indicador> indiss = indiCon.findIndicadorEntities();
        List<String> Lindis = new ArrayList<>();
        for(Indicador i:indiss){
            if(i.getIndicadorObjetivo().equals(obj)){
                Lindis.add(i.getDescripcion());
            }
        }
        return Lindis;
    }
     public boolean eliminarIndicador(Indicador indi) {
        try {
            indiCon.destroy(indi.getCodigo());
            return true;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ObjetivoLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
     public Indicador getIndicador2(String descp) {
        List<Indicador> metss = indiCon.findIndicadorEntities();
        for(Indicador u:metss){
            if(u.getDescripcion().equals(descp))
                return u;
        }
        return null;
    }

    public void editarIndicador(Indicador indi, Usuario userCreador, Objetivo obj, String area, String text) {
        indi.setDescripcion(text);
        try {
            indiCon.edit(indi);
            JOptionPane.showMessageDialog(null, "Indicador editada con exito");
        } catch (Exception ex) {
            Logger.getLogger(MetaLogica.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error al editar Indicador");
        }
    }
}
