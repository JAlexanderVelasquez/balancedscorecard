/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
 
import modelo.Usuario;
import persistencia.UsuarioJpaController;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
 
public class UsuarioLogica {
    
    private UsuarioJpaController usuCon=new UsuarioJpaController();
    
    public UsuarioLogica(){
        
    }
    
    public Usuario getUsuario(String di, String pass){
        Usuario usu=usuCon.findUsuario(di);
        if(usu==null)
            JOptionPane.showMessageDialog(null, "Error, Usuario no registrado");
        else if(!usu.getContrasena().equals(pass))
            JOptionPane.showMessageDialog(null, "Error, Contraseña incorrecta");
        
        if(usu!=null && usu.getContrasena().equals(pass))
            return usu;
        return null;
    }
    public List<Usuario> listaUsuarios(){
        return usuCon.findUsuarioEntities();
    }
    public boolean verificarUsuario(String di){
        Usuario usu=usuCon.findUsuario(di);
        if(usu==null){
            return false;
        }
        return true;
    }
     public boolean registrarUsuario(String di, String nom, String ape, String email, String dir, String tel, String pass,String tipo){
        Usuario usu=new Usuario();
        usu.setCedula(di);
        usu.setNombre(nom);
        usu.setApellido(ape);
        usu.setEmail(email);
        usu.setDireccion(dir);
        usu.setTelefono(tel);
        usu.setContrasena(pass);
        usu.setRol(tipo);
        usu.setEstado("ACTIVO");
        try {
            usuCon.create(usu);
            JOptionPane.showMessageDialog(null, "Usuario registrado con exito");
            return false;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al registrar usuario");
        }
        
        return true;
        
    }
     public List<String> obtenerUsuarios(){
        List<Usuario> userss = usuCon.findUsuarioEntities();
        List<String> Lusers = new ArrayList<>();
        for(Usuario u:userss){
            Lusers.add(""+u.getCedula());
        }
        return Lusers;
    }
    public Usuario obtenerUsuarios(String cod){
        return usuCon.findUsuario(cod);
    }

    public boolean editarUsuario(Usuario usuA,String cc,String pass,String rol, String estado, 
            String nom, String ape, String tel, String email, String direc) {
        
        usuA.setCedula(cc);
        usuA.setContrasena(pass);
        usuA.setRol(rol);
        usuA.setEstado(estado);
        usuA.setNombre(nom);
        usuA.setApellido(ape);        
        usuA.setTelefono(tel);
        usuA.setEmail(email);
        usuA.setDireccion(direc);

        try {
            usuCon.edit(usuA);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(UsuarioLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(UsuarioLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean eliminarUsuario(Usuario us) {
        try {
            us.setEstado("INACTIVO");
            usuCon.edit(us);
            
            return true;
            
            
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(UsuarioLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(UsuarioLogica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}

