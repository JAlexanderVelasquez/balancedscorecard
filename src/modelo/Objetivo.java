/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "objetivo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objetivo.findAll", query = "SELECT o FROM Objetivo o")
    , @NamedQuery(name = "Objetivo.findById", query = "SELECT o FROM Objetivo o WHERE o.id = :id")
    , @NamedQuery(name = "Objetivo.findByDescripcion", query = "SELECT o FROM Objetivo o WHERE o.descripcion = :descripcion")
    , @NamedQuery(name = "Objetivo.findByArea", query = "SELECT o FROM Objetivo o WHERE o.area = :area")
    , @NamedQuery(name = "Objetivo.findByFecha", query = "SELECT o FROM Objetivo o WHERE o.fecha = :fecha")})
public class Objetivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "area")
    private String area;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "indicadorObjetivo")
    private List<Indicador> indicadorList;
    @JoinColumn(name = "creador_objetivo", referencedColumnName = "Cedula")
    @ManyToOne(optional = false)
    private Usuario creadorObjetivo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iniciativaObjetivo")
    private List<Iniciativa> iniciativaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metaObjetivo")
    private List<Meta> metaList;

    public Objetivo() {
    }

    public Objetivo(Integer id) {
        this.id = id;
    }

    public Objetivo(Integer id, String descripcion, String area) {
        this.id = id;
        this.descripcion = descripcion;
        this.area = area;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public List<Indicador> getIndicadorList() {
        return indicadorList;
    }

    public void setIndicadorList(List<Indicador> indicadorList) {
        this.indicadorList = indicadorList;
    }

    public Usuario getCreadorObjetivo() {
        return creadorObjetivo;
    }

    public void setCreadorObjetivo(Usuario creadorObjetivo) {
        this.creadorObjetivo = creadorObjetivo;
    }

    @XmlTransient
    public List<Iniciativa> getIniciativaList() {
        return iniciativaList;
    }

    public void setIniciativaList(List<Iniciativa> iniciativaList) {
        this.iniciativaList = iniciativaList;
    }

    @XmlTransient
    public List<Meta> getMetaList() {
        return metaList;
    }

    public void setMetaList(List<Meta> metaList) {
        this.metaList = metaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objetivo)) {
            return false;
        }
        Objetivo other = (Objetivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Objetivo[ id=" + id + " ]";
    }
    
}
