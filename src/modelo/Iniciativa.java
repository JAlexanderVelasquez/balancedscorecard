/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "iniciativa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Iniciativa.findAll", query = "SELECT i FROM Iniciativa i")
    , @NamedQuery(name = "Iniciativa.findByCodigo", query = "SELECT i FROM Iniciativa i WHERE i.codigo = :codigo")
    , @NamedQuery(name = "Iniciativa.findByDescripcion", query = "SELECT i FROM Iniciativa i WHERE i.descripcion = :descripcion")
    , @NamedQuery(name = "Iniciativa.findByFecha", query = "SELECT i FROM Iniciativa i WHERE i.fecha = :fecha")
    , @NamedQuery(name = "Iniciativa.findByArea", query = "SELECT i FROM Iniciativa i WHERE i.area = :area")})
public class Iniciativa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codigo")
    private Integer codigo;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "Area")
    private String area;
    @JoinColumn(name = "iniciativa_objetivo", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Objetivo iniciativaObjetivo;

    public Iniciativa() {
    }

    public Iniciativa(Integer codigo) {
        this.codigo = codigo;
    }

    public Iniciativa(Integer codigo, String descripcion, String area) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.area = area;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Objetivo getIniciativaObjetivo() {
        return iniciativaObjetivo;
    }

    public void setIniciativaObjetivo(Objetivo iniciativaObjetivo) {
        this.iniciativaObjetivo = iniciativaObjetivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Iniciativa)) {
            return false;
        }
        Iniciativa other = (Iniciativa) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Iniciativa[ codigo=" + codigo + " ]";
    }
    
}
