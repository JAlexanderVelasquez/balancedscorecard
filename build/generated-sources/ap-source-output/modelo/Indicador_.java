package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Objetivo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-23T19:04:32")
@StaticMetamodel(Indicador.class)
public class Indicador_ { 

    public static volatile SingularAttribute<Indicador, String> descripcion;
    public static volatile SingularAttribute<Indicador, String> area;
    public static volatile SingularAttribute<Indicador, Date> fecha;
    public static volatile SingularAttribute<Indicador, Integer> codigo;
    public static volatile SingularAttribute<Indicador, Objetivo> indicadorObjetivo;

}