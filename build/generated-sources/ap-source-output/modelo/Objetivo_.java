package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Indicador;
import modelo.Iniciativa;
import modelo.Meta;
import modelo.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-23T19:04:32")
@StaticMetamodel(Objetivo.class)
public class Objetivo_ { 

    public static volatile SingularAttribute<Objetivo, String> descripcion;
    public static volatile SingularAttribute<Objetivo, String> area;
    public static volatile SingularAttribute<Objetivo, Date> fecha;
    public static volatile ListAttribute<Objetivo, Meta> metaList;
    public static volatile SingularAttribute<Objetivo, Usuario> creadorObjetivo;
    public static volatile ListAttribute<Objetivo, Iniciativa> iniciativaList;
    public static volatile ListAttribute<Objetivo, Indicador> indicadorList;
    public static volatile SingularAttribute<Objetivo, Integer> id;

}