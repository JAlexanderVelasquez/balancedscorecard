package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Objetivo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-23T19:04:32")
@StaticMetamodel(Meta.class)
public class Meta_ { 

    public static volatile SingularAttribute<Meta, String> descripcion;
    public static volatile SingularAttribute<Meta, String> area;
    public static volatile SingularAttribute<Meta, Date> fecha;
    public static volatile SingularAttribute<Meta, Integer> codigo;
    public static volatile SingularAttribute<Meta, Objetivo> metaObjetivo;

}