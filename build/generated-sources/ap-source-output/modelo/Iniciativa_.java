package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Objetivo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-10-23T19:04:32")
@StaticMetamodel(Iniciativa.class)
public class Iniciativa_ { 

    public static volatile SingularAttribute<Iniciativa, String> descripcion;
    public static volatile SingularAttribute<Iniciativa, String> area;
    public static volatile SingularAttribute<Iniciativa, Date> fecha;
    public static volatile SingularAttribute<Iniciativa, Integer> codigo;
    public static volatile SingularAttribute<Iniciativa, Objetivo> iniciativaObjetivo;

}